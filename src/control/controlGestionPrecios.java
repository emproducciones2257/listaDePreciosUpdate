package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import conexionBD.DBDtos;
import conexionBD.DBGestionCategorias;
import conexionBD.dbGestionPrecios;
import modelo.categorias;
import modelo.constantes;
import modelo.dtosNecesarios;
import modelo.preciosDocumento;
import views.PnlMauroSergio;
import views.Principal;
import views.pnlGestionPrecios;
import views.ventanasAvisos;

public class controlGestionPrecios implements ActionListener, MouseListener, KeyListener {

	private pnlGestionPrecios pnlPrecios;
	//private File archivo = new File("");
	private ArrayList<preciosDocumento> precios;
	private dbGestionPrecios DBGP;
	private DBDtos DBDT;
	private ventanasAvisos avisos;
	private String fecha, categoriaSeleccionada = "";
	private categorias indiceCatSelec;
	private preciosDocumento productoActualizar;
	private List<preciosDocumento> prepre;
	private ArrayList<categorias> categorias;
	private DBGestionCategorias DBCategorias;
	private FileNameExtensionFilter filPdf, filExcel1, filExcel2;

	public controlGestionPrecios(pnlGestionPrecios pnlPrecios) {
		this.pnlPrecios = pnlPrecios;
		pnlPrecios.getBtnProcesar().addActionListener(this);
		pnlPrecios.getTblListadoPrecios().addMouseListener(this);
		pnlPrecios.getBtnRegistrarPrecio().addActionListener(this);
		pnlPrecios.getTxtFiltrarProducto().addKeyListener(this);
		pnlPrecios.getJcmbCategorias().addActionListener(this);
		pnlPrecios.getBtnMrioSer().addActionListener(this);
		precios = new ArrayList<preciosDocumento>();
		DBGP = new dbGestionPrecios();
		DBCategorias = new DBGestionCategorias();
		DBDT = new DBDtos();
		avisos = new ventanasAvisos(pnlPrecios);
		cargarCategorias();
		precios = new ArrayList<>();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(pnlPrecios.getBtnBuscarArchivo())) {
			obtenerArchivo();
		}

		if (e.getSource().equals(pnlPrecios.getBtnProcesar())) {

			if ((pnlPrecios.getTxtPorcentaje().getText().isEmpty())) {
				avisos.faltanDatos(ventanasAvisos.FALTAN_DATOS);
			}

			if (!pnlPrecios.getTxtPorcentaje().getText().isEmpty()) {
				enviarArchivoBD(0);			
				pnlPrecios.getLblEstadoArchivo().setText("Sin Archivo");
			}

			if ((!pnlPrecios.getTxtPorcentaje().getText().isEmpty())) {
				DBDT.actualiazarPorcentaje(Integer.parseInt(pnlPrecios.getTxtPorcentaje().getText()));
			}

			if ( (!pnlPrecios.getTxtPorcentaje().getText().isEmpty())) {

				enviarArchivoBD(Integer.parseInt(pnlPrecios.getTxtPorcentaje().getText()));

				
				pnlPrecios.getLblEstadoArchivo().setText("Sin Archivo");
			}
		}

		if (e.getSource().equals(pnlPrecios.getBtnRegistrarPrecio())) {

			if (!pnlPrecios.getTxtPrecio().getText().isEmpty()) {
				productoActualizar.setPrecio(Double.parseDouble(pnlPrecios.getTxtPrecio().getText()));
			}

			if (!pnlPrecios.getTxtDescripcion().getText().isEmpty()) {
				productoActualizar.setProd(pnlPrecios.getTxtDescripcion().getText());
			}

			DBGP.actualizarRegistro(productoActualizar);
		}

		if (e.getSource().equals(pnlPrecios.getJcmbCategorias())) {

			categoriaSeleccionada = pnlPrecios.getJcmbCategorias().getSelectedItem().toString();

			if (!categoriaSeleccionada.equals(constantes.VALOR_DEFECTO_CATEGORIAS)) {
				indiceCatSelec = categorias.get(pnlPrecios.getJcmbCategorias().getSelectedIndex() - 1);
			} else {

				if (pnlPrecios.getTblListadoPrecios().getRowCount() > 0) {
					pnlPrecios.limpiarTabla();
				}
			}

			if (!categoriaSeleccionada.equals(constantes.VALOR_DEFECTO_CATEGORIAS)) {

				pnlPrecios.getBtnBuscarArchivo().setEnabled(true);

				if (categoriaSeleccionada.equals("LIBRERIA")) {
					mostrarTodosPreciosPorCategorias();
					// Guardo las preferencias de la extencion
					filPdf = new FileNameExtensionFilter(constantes.VALOR_TIPO_PDF, constantes.VALOR_EXTENCION_PDF);

				} else {
					mostrarTodosPreciosPorCategorias();

					filExcel1 = new FileNameExtensionFilter(constantes.VALOR_TIPO_EXCEL1, constantes.VALOR_EXTENCION_EXCEL1);
					filExcel2 = new FileNameExtensionFilter(constantes.VALOR_TIPO_EXCEL2, constantes.VALOR_EXTENCION_EXCEL2);
				}

			} else {
				pnlPrecios.getBtnBuscarArchivo().setEnabled(false);
			}
		}

		if (e.getSource().equals(pnlPrecios.getBtnMrioSer())) {
			PnlMauroSergio pnl = new PnlMauroSergio();
			pnl.setVisible(true);
		}
	}

	private void mostrarTodosPreciosPorCategorias() {
		prepre = DBGP.obtenerListadoProductosPrecios(indiceCatSelec.getIdCategoria());

		if (!prepre.isEmpty()) {

			if (pnlPrecios.getTblListadoPrecios().getRowCount() > 0) {
				pnlPrecios.limpiarTabla();
			}
			pnlPrecios.setLista(prepre);
			pnlPrecios.modeloTabla();
		}
	}

	private void enviarArchivoBD(int porcentaje) {

		if (categoriaSeleccionada.equals("LIBRERIA")) {
			if (extraerTextoPdf()) {
				DBDT.actualiazarDtos(fecha, porcentaje);
				Principal.refrescarDatos();

				DBGP.cargarADB(precios, categoriaSeleccionada);
				mostrarTodosPreciosPorCategorias();
			} else {
				avisos.errorUpdate(ventanasAvisos.NO_ES_NECESARIO_ACTUALIZAR, categoriaSeleccionada);
			}
			
		} else {
			//extraerDtosExcel(archivo);
		}

		
	}

	private void cargarCategorias() {
		categorias = DBCategorias.obtenerCategorias();

		if (pnlPrecios.getJcmbCategorias().getItemCount() > 0) {
			pnlPrecios.getJcmbCategorias().removeAllItems();
		}

		pnlPrecios.getJcmbCategorias().addItem(constantes.VALOR_DEFECTO_CATEGORIAS);
		if (!categorias.isEmpty()) {

			for (categorias ca : categorias) {
				pnlPrecios.getJcmbCategorias().addItem(ca.getNomCat());
			}
		}
	}

	private void obtenerArchivo() {

		JFileChooser selectorArchivo = new JFileChooser();
		File defaultDirectory = new File("C:\\Users\\Emanuel\\Desktop");
        selectorArchivo.setCurrentDirectory(defaultDirectory);

		if (categoriaSeleccionada.equals("LIBRERIA")) {
			selectorArchivo.setFileFilter(filPdf);
		} else {
			selectorArchivo.setFileFilter(filExcel1);
			selectorArchivo.setFileFilter(filExcel2);
		}

		int returnVal = selectorArchivo.showOpenDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			//archivo = selectorArchivo.getSelectedFile().getAbsoluteFile();
			pnlPrecios.getLblEstadoArchivo().setText(constantes.ARCHIVO_OK);
		} else {
			avisos.CargaErronea(ventanasAvisos.ERROR_CARGA_ARCHIVO);
		}
	}

	public PDDocument downloadPDF(String urlString) {
        PDDocument document = null;
        InputStream inputStream = null;

        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = connection.getInputStream();
                byte[] pdfBytes = IOUtils.toByteArray(inputStream);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(pdfBytes);
                document = PDDocument.load(byteArrayInputStream);
            } else {
                System.err.println("Error: Failed to download PDF. HTTP response code: " + connection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return document;
    }

	private boolean extraerTextoPdf() {
		boolean resultado = true;
		try {

			// cargo el documento
			PDDocument documento = downloadPDF(ventanasAvisos.URL_CASA_JUJUY_PDF);
			
			// creo una instancia de la clase que me permite extraer el texto del pdf

			PDFTextStripper extraccion = new PDFTextStripper();

			String textoRecuperado = extraccion.getText(documento);		

			fecha = obtenerFechaDocumento(documento);

			dtosNecesarios datosNecesarios = DBDT.obtenerRegistro();

			if (compararFechas(fecha, datosNecesarios.getFechaBD())) {
				String textoSinEncabezado = quitarEncabezado(textoRecuperado, fecha);
				pruebaDelimitador(textoSinEncabezado);
			}  else {
				resultado = false;
			}
			
			documento.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return resultado;
	}

	private boolean compararFechas (String fechaUno, String fechaDos) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		java.util.Date date1 = dateFormat.parse(fechaUno);
        java.util.Date date2 = dateFormat.parse(fechaDos);

		if (date1.after(date2)) {
			return true;
		} else {
			return false;
		}
	}

	private void extraerDtosExcel(File ruta) {

		try {
			FileInputStream documentoExcel = new FileInputStream(ruta);
			// creo libro
			Workbook libroExcell = WorkbookFactory.create(documentoExcel);

			// obtengo la primero hoja del libro
			Sheet HojaLibro = libroExcell.getSheetAt(0);

			ArrayList<Object> filasCelda;

			Cell celdaTemporal = null;

			Iterator<Row> filaIterator = HojaLibro.rowIterator();
			filaIterator.next();

			while (filaIterator.hasNext()) {

				Row fila = filaIterator.next();

				Iterator<Cell> celdalIterator = fila.cellIterator();

				filasCelda = new ArrayList<>();

				while (celdalIterator.hasNext()) {
					celdaTemporal = celdalIterator.next();

					if (celdaTemporal.getCellType() == CellType.NUMERIC) {
						filasCelda.add(celdaTemporal.getNumericCellValue());
					}
					if (celdaTemporal.getCellType() == CellType.STRING) {
						filasCelda.add(celdaTemporal.getStringCellValue());
					}
				}
				procesarCelda(filasCelda);
			}

			documentoExcel.close();
			libroExcell.close();
		} catch (Exception e) {
			avisos.CargaErronea(ventanasAvisos.ERROR_CARGA_ARCHIVO);
			e.printStackTrace();
		}
	}

	private void procesarCelda(ArrayList<Object> filasCelda) {
		if (filasCelda.size() == 3) {
			preciosDocumento pTemp = new preciosDocumento(filasCelda.get(0).toString(), filasCelda.get(1).toString(), Double.valueOf(filasCelda.get(2).toString()),
					indiceCatSelec.getIdCategoria());
			precios.add(pTemp);
		}
	}

	private String obtenerFechaDocumento(PDDocument documento) throws IOException {
		String dateTime = null;
		String regex = "^\\d{1,2}/\\d{1,2}/\\d{4} \\d{2}:\\d{2}:\\d{2}";
		
			
			// Usar PDFTextStripper para extraer el texto de la página
	        PDFTextStripper textStripper = new PDFTextStripper();
	        textStripper.setStartPage(1); // Las páginas en PDFBox comienzan desde 1
	        textStripper.setEndPage(1);

	        String pageText = textStripper.getText(documento);

	        // Dividir el texto en líneas y obtener la última línea
	        List<String> lines = Arrays.asList(pageText.split("\r\n|\r|\n"));
	        String lastLine = lines.get(lines.size() - 1);
	        
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(lastLine);
	        
	        if (matcher.find()) {
	            dateTime = matcher.group();
	        }	              
		return dateTime;
	}

	private String quitarEncabezado(String t, String fecha) {
		String textoListo = "";

		String temp;

		Scanner escaner = new Scanner(t);

		while (escaner.hasNext()) {
			temp = escaner.nextLine();

			if ((temp).equals("https://casajujuy.com/Casa Jujuy") || temp.contains(fecha)) {
				if (escaner.hasNext()) {
					escaner.nextLine();
				} else break;

				if (escaner.hasNext()) {
					escaner.nextLine();
				} else break;

				if (escaner.hasNext()) {
					escaner.nextLine();
				} else break;

				if (escaner.hasNext()) {
					escaner.nextLine();
				} else break;

				temp = escaner.nextLine();
				if (temp.equals("Codigo Producto Precio")) {
					temp = escaner.nextLine();	
				}
				textoListo = textoListo + temp + "\n";
			} else {
				textoListo = textoListo + temp + "\n";
			}
		}
		escaner.close();
		return textoListo;
	}

	private void pruebaDelimitador(String listaArticulos) throws IOException {

		Scanner elEscaner = new Scanner(listaArticulos);
		String codigo = "";
		String descripcion = "";
		String precio = "";
		String linea = "";
		String segundaLinea = "";
		String terceraLinea = "";

		while (elEscaner.hasNext()) {

			linea = elEscaner.nextLine();

			if (linea.contains("$")) {
				// Extraer el código (primer número en la cadena)
				codigo = linea.substring(0, linea.indexOf(" "));

				// Extraer el precio (número después del símbolo $)
				precio = linea.substring(linea.indexOf("$") + 1);

				// Extraer la descripción (lo que queda después del código y antes del precio)
				descripcion = linea.substring(codigo.length(), linea.indexOf("$")).trim();
			} else {
				segundaLinea = elEscaner.nextLine();
				terceraLinea = elEscaner.nextLine();

				// Extraer el código (primer número en la cadena)
				codigo = linea.substring(0, linea.indexOf(" "));

				// Extraer el precio (número después del símbolo $)
				precio = terceraLinea.substring(linea.indexOf("$") + 2);

				// Extraer la descripción (lo que queda después del código y antes del precio)
				descripcion = linea.substring(codigo.length(), linea.length()).trim() + " " + segundaLinea;
			}

			preciosDocumento temp = new preciosDocumento(codigo, descripcion, Double.parseDouble(precio), indiceCatSelec.getIdCategoria());

			precios.add(temp);

			codigo = "";
			descripcion = "";
			precio = "";
			linea = "";
			segundaLinea = "";
			terceraLinea = "";
		}
		elEscaner.close();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		productoActualizar = pnlPrecios.retornarElemento(pnlPrecios.getTblListadoPrecios().getSelectedRow());
		pnlPrecios.getTxtIdProducto().setText(productoActualizar.getIdPrecio() + "");
		pnlPrecios.getTxtDescripcion().setText(productoActualizar.getProd());
		pnlPrecios.getTxtPrecio().setText(productoActualizar.getPrecio() + "");
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

		/*
		 * prepre = DBGP.obtenerListadoProductosPreciosFiltrados(pnlPrecios.getTxtFiltrarProducto().getText());
		 * 
		 * if (!prepre.isEmpty()) { pnlPrecios.limpiarTabla(); pnlPrecios.setLista(prepre); pnlPrecios.modeloTabla(); }else { pnlPrecios.limpiarTabla(); }
		 */
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
